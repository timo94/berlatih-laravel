<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Akun SanberBook</title>
</head>
<body>
    <div>
        <h1>Buat Account Baru!</h1>
    </div>
    <div>
        <h2>Sign Up Form</h2>
        <form action="/welcome" method="POST">
            @csrf
            <label for="first_name">First name:</label>
            <br>
            <br>
            <input type="text" required placeholder="First Name" id="first_name" name="first_name">
            <br>
            <br>
            <label for="last_name">Last name:</label>
            <br>
            <br>
            <input type="text" required placeholder="Last Name" id="last_name" name="last_name">
            <br>
            <br>
            
            <label>Gender:</label><br><br>
            <input type="radio" name="gender" id="male" value="0" checked><label for="male">Male</label> <br>
            <input type="radio" name="gender" id="female" value="1"><label for="female">Female</label>  <br>
            <input type="radio" name="gender" id="other" value="2"><label for="other">Other</label> <br>
            <br>
            
            <label>Nationality:</label>
            <select name="nationality">
                <option value="id">Indonesian</option>
                <option value="my">Malaysian</option>
                <option value="sg">Singaporean</option>
                <option value="au">Australian</option>
            </select>
            <br><br>
            
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="language[]" id="in" value="id"><label for="in">Bahasa Indonesia </label> <br>
            <input type="checkbox" name="language[]" id="en" value="en"><label for="en">English </label> <br>
            <input type="checkbox" name="language[]" id="ot" value="ot"><label for="ot">Other </label> <br>
            <br>
            
            <label for="bio">Bio:</label><br><br>
            <textarea cols="100", rows="7" placeholder="Bio" id="bio" name="bio"></textarea>
            <br><br>
            <input type="submit" value="Sign Up">
        </form>
    </div>
</body>
</html>