<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form(){
        return view('register');
    }

    public function welcome(Request $request){
        return "welcome";
    }

    public function welcome_post(Request $request){
        //dd($request->all());
        $first_name = $request["first_name"] ? $request["first_name"] : "";
        $last_name = $request["last_name"] ? $request["last_name"] : "";
        if (!$first_name OR !$last_name){
            echo "<script type='text/javascript'>alert('Required Fields is empty');</script>";
            return view('register');
        }
        return view('welcome', ["first_name"=> $first_name, "last_name"=>$last_name]);
    }
}
